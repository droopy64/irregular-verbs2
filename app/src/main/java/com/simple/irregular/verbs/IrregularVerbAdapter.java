    package com.simple.irregular.verbs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class IrregularVerbAdapter extends RecyclerView.Adapter<IrregularVerbAdapter.IrregularVerbHolder> implements Filterable {

    private List<IrregularVerb> irregularVerbsList = new ArrayList<>();
    private List<IrregularVerb> irregularVerbsListFull;

    public IrregularVerbAdapter() {
    }

    /**
     * Called when RecyclerView needs a new {@link RecyclerView.ViewHolder} of the given type to represent
     * an item.
     */
    @NonNull
    @Override
    public IrregularVerbHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.irregularverb_item, parent, false);
        return new IrregularVerbHolder(itemView);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link RecyclerView.ViewHolder#itemView} to reflect the item at the given
     * position.
     */
    @Override
    public void onBindViewHolder(@NonNull IrregularVerbHolder holder, int position) {
        IrregularVerb currentIrregularVerb = irregularVerbsList.get(position);
        holder.textViewInfinitive.setText(currentIrregularVerb.getInfinitive());
        holder.textViewPastSimple.setText(currentIrregularVerb.getPastSimple());
        holder.textViewPastParticiple.setText(currentIrregularVerb.getPastParticiple());

    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return irregularVerbsList.size();
    }

    public void setIrregularVerbsList(List<IrregularVerb> irregularVerbsList) {
        this.irregularVerbsList = irregularVerbsList;
        notifyDataSetChanged();
    }

    public void setIrregularVerbsListFull() {
        this.irregularVerbsListFull = new ArrayList<>(irregularVerbsList);
        //notifyDataSetChanged();
    }

    class IrregularVerbHolder extends RecyclerView.ViewHolder {
        private TextView textViewInfinitive;
        private TextView textViewPastSimple;
        private TextView textViewPastParticiple;

        public IrregularVerbHolder(@NonNull View itemView) {
            super(itemView);
            textViewInfinitive = itemView.findViewById(R.id.text_view_infinitive);
            textViewPastSimple = itemView.findViewById(R.id.text_view_past_simple);
            textViewPastParticiple = itemView.findViewById(R.id.text_view_past_participle);
        }
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * <p>This method is usually implemented by {@link Adapter}
     * classes.</p>
     *
     * @return a filter used to constrain data
     */
    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<IrregularVerb> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(irregularVerbsListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (IrregularVerb item: irregularVerbsListFull) {
                    if (item.getInfinitive().toLowerCase().contains(filterPattern)
                            || item.getPastSimple().toLowerCase().contains(filterPattern)
                            || item.getPastParticiple().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);

                    }
                    
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            irregularVerbsList.clear();
            irregularVerbsList.addAll((List<IrregularVerb>) results.values);
            notifyDataSetChanged();
        }
    };
}
