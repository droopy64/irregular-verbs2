package com.simple.irregular.verbs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;

import static com.simple.irregular.verbs.AdsManager.rewardedAd;
import static com.simple.irregular.verbs.AdsManager.setRewardedAdShown;
import static com.simple.irregular.verbs.MainActivity.coffee;

public class SIVDialog extends AppCompatDialogFragment {
    private static final String TAG = "SIVDialog";

    private final Context mContext = MainActivity.getAppContext();

    public static DialogType mSource;

    public SIVDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static SIVDialog newInstance(String title, String message, String positiveButton,
                                        String negativeButton, Enum<?> dialogNegativeAction,
                                        DialogType dialogSource) {
        SIVDialog frag = new SIVDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("positiveButton", positiveButton);
        args.putString("negativeButton", negativeButton);
        args.putSerializable("dialogNegativeAction", dialogNegativeAction);
        frag.setArguments(args);

        mSource = dialogSource;

        return frag;
    }

    @NonNull
    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {

        FirebaseHelper firebaseHelper = new FirebaseHelper();

        Bundle args = getArguments();

        String title = args.getString("title", "This is default title");
        String message = args.getString("message", "This is default message");
        String positiveButton = args.getString("positiveButton", "OK");
        String negativeButton = args.getString("negativeButton", "Cancel");
        DialogActions dialogNegativeAction = (DialogActions) args.getSerializable("dialogNegativeAction");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButton, (dialog, which) -> {
                    if (rewardedAd.isLoaded()) {
                        Activity activityContext = getActivity();
                        RewardedAdCallback adCallback = new RewardedAdCallback() {
                            @Override
                            public void onRewardedAdOpened() {
                                // Ad opened.
                                Log.d(TAG, "onRewardedAdOpened: Ad opened");
                            }

                            @Override
                            public void onRewardedAdClosed() {
                                // Ad closed.
                                Log.d(TAG, "onRewardedAdClosed: Ad closed");
                                AdsManager.createRewardedAds();
                            }

                            @Override
                            public void onUserEarnedReward(@NonNull RewardItem reward) {
                                // User earned reward.

                                Log.d(TAG, "onUserEarnedReward: User earned reward" + reward);
                                Log.d(TAG, "Counter " + coffee.getCounter());
                                coffee.addCoffee();
                                setRewardedAdShown(true);
                                Log.d(TAG, "Counter++ " + coffee.getCounter());

                                firebaseHelper.setUserProperty("shouted_me_coffee", "Yes");
                                firebaseHelper.setUserProperty("shouted_coffees_balance", String.valueOf(coffee.getCounter()));

                                //TODO: IF user comes from MODAL A then ITEM_ID...

                                firebaseHelper.logAdSource(String.valueOf(mSource));
                                firebaseHelper.logScreenName(String.valueOf(mSource), TAG);
                                activityContext.invalidateOptionsMenu();

                                //TODO: Here coffee counter can be incremented + maybe visually presented for a moment?
                            }

                            @Override
                            public void onRewardedAdFailedToShow(AdError adError) {
                                // Ad failed to display.
                                Log.d(TAG, "onRewardedAdFailedToShow: Ad failed to display");
                            }
                        };
                        rewardedAd.show(activityContext, adCallback);
                    } else {
                        Log.d(TAG, "The rewarded ad wasn't loaded yet.");
                    }
                })
                .setNegativeButton(negativeButton, (dialog, which) -> {
                    if (dialog != null && getDialog().isShowing()) {
                        if (dialogNegativeAction == DialogActions.CANCEL){
                            //Close modal

                            firebaseHelper.logButtonItem("cancel_coffee_modal");
                            dialog.dismiss();
                        }
                        else {
                            //Close application
                            firebaseHelper.logButtonItem("exit_app");
                            getActivity().finish();
                        }
                    }
                    else {
                        Log.d(TAG, "Dialog not shown");
                    }
                });
        return builder.create();
    }

}