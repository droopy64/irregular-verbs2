package com.simple.irregular.verbs;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface IrregularVerbDao {

    @Insert
    void insert(IrregularVerb irregularVerb);

    @Update
    void update(IrregularVerb irregularVerb);

    @Delete
    void delete(IrregularVerb irregularVerb);

    @Query("DELETE FROM irregularVerbs_table")
    void deleteAllIrregularVerbs();

    @Query("SELECT * FROM irregularVerbs_table ORDER BY infinitive ASC")
    LiveData<List<IrregularVerb>> getAllIrregularVerbs();
}
