package com.simple.irregular.verbs;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.ads.AdView;

import static com.simple.irregular.verbs.AdsManager.createRewardedAds;
import static com.simple.irregular.verbs.AdsManager.setRewardedAdShown;
import static com.simple.irregular.verbs.AdsManager.wasRewardedAdShown;
import static com.simple.irregular.verbs.DialogType.DIALOG_BUY_A_COFFEE;
import static com.simple.irregular.verbs.DialogType.DIALOG_EXIT_APP;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    AdView bannerAd;

    private static Context mContext;

    private FirebaseHelper mFirebaseHelper;

    private IrregularVerbViewModel irregularVerbViewModel;

    private IrregularVerbAdapter adapter;

    public static Coffee coffee = new Coffee();

    Tools screenSaver;

    private SearchView searchView;
    private String mSearchString;

    private Intent savedUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Life Cycle: onCreate()");

        screenSaver = new Tools(this);

        mContext = getApplicationContext();

        mFirebaseHelper = new FirebaseHelper();

        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        // recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);

        adapter = new IrregularVerbAdapter();
        recyclerView.setAdapter(adapter);

        irregularVerbViewModel = new ViewModelProvider(this).get(IrregularVerbViewModel.class);
        irregularVerbViewModel.getAllIrregularVerbs().observe(this, irregularVerbs -> {
            adapter.setIrregularVerbsList(irregularVerbs);
            adapter.setIrregularVerbsListFull();
        });

        // Banner ad
        bannerAd = findViewById(R.id.adView_banner_on_the_bottom);
        AdsManager adsManager = new AdsManager(this);
        adsManager.createAds(bannerAd);

        //Rewarded ad
        createRewardedAds();

        screenSaver.isScreenOn();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "Life Cycle: onStart()");
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Life Cycle: onResume()");
        if (!screenSaver.wasScreenDimmed() && !wasRewardedAdShown()) {
            coffee.removeCoffee();
        }
        screenSaver.setScreenDimmed(false);
        setRewardedAdShown(false);

        // restore & focus on the SearchView
        if (savedUserData != null) {
            Log.d(TAG, "onResume(): There is something to restore");
            mSearchString = getIntent().getStringExtra("SEARCH_KEY");

            Log.d(TAG, "onResume(): mSearchString retrived");
            searchView.setQuery(mSearchString, true);
            searchView.clearFocus();
            Log.d(TAG, "onResume(): mSearchString retrived value " + mSearchString);
        } else {
            Log.d(TAG, "onResume(): There is NOTHING to restore");
        }
    }

    /**
     * Dispatch onPause() to fragments.
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "Life Cycle: onPause()");
        screenSaver.isScreenOn();

        if (screenSaver.isScreenOn() == false) {
            screenSaver.setScreenDimmed(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "Life Cycle: onStop()");
        screenSaver.isScreenOn();

        if (searchView != null) {
            mSearchString = searchView.getQuery().toString();
            savedUserData = getIntent().putExtra("SEARCH_KEY", mSearchString);
        } else {
            Log.i(TAG, "Life Cycle: onStop(): searchView = null thus data savedUserData not restorable");
        }
    }

    /**
     * Called after {@link #onStop} when the current activity is being
     * re-displayed to the user (the user has navigated back to it).  It will
     * be followed by {@link #onStart} and then {@link #onResume}.
     *
     * <p>For activities that are using raw {@link Cursor} objects (instead of
     * creating them through
     * {@link #managedQuery(Uri, String[], String, String[], String)},
     * this is usually the place
     * where the cursor should be requeried (because you had deactivated it in
     * {@link #onStop}.
     *
     * <p><em>Derived classes must call through to the super class's
     * implementation of this method.  If they do not, an exception will be
     * thrown.</em></p>
     *
     * @see #onStop
     * @see #onStart
     * @see #onResume
     */
    @Override
    protected void onRestart() {
        Log.i(TAG, "Life Cycle: onRestart()");
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Life Cycle: onDestroy()");
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.topbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();

        //change magnifier icon on onscreen keyboard to "done" icon
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i(TAG, "onQueryTextChange() START newText: " + newText);
                    mSearchString = newText;
                    adapter.getFilter().filter(mSearchString);
                Log.i(TAG, "onQueryTextChange() END: " + mSearchString);
                return true;
            }
        });

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                invalidateOptionsMenu();
                Log.i(TAG, "onMenuItemActionCollapse: CALLED");
                return true;
            }
        });

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        if (menuItem == R.id.action_buy_a_coffee) {
            openDialog(DIALOG_BUY_A_COFFEE);
            //Firebase
            mFirebaseHelper.logButtonItem("coffee_cup_top_app_bar");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int sumOfCoffees = coffee.getCounter();
        Log.d(TAG, "sumOfCoffees : " + sumOfCoffees);
        setCount(this, sumOfCoffees, menu);
        Log.i(TAG, "onPrepareOptionsMenu: CALLED");

        return true;
    }

    public void setCount(Context context, int count, Menu menu) {

        MenuItem menuItem = menu.findItem(R.id.action_buy_a_coffee);
        LayerDrawable icon = (LayerDrawable) menuItem.getIcon();

        CountDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_coffee_count);
        if (reuse != null && reuse instanceof CountDrawable) {
            badge = (CountDrawable) reuse;
        } else {
            badge = new CountDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_coffee_count, badge);
    }

    @Override
    public void onBackPressed() {
        openDialog(DIALOG_EXIT_APP);
    }

    public void openDialog(DialogType tag) {
        FragmentManager fm = getSupportFragmentManager();
        SIVDialog dialog = null;
        switch (tag) {
            case DIALOG_BUY_A_COFFEE:
                dialog = SIVDialog.newInstance(
                        getString(R.string.title_do_you_like_the_app),
                        getString(R.string.modal_coffee_explanation),
                        getString(R.string.button_open_advert),
                        getString(R.string.button_cancel),
                        DialogActions.CANCEL,
                        DIALOG_BUY_A_COFFEE);
                mFirebaseHelper.logModalShows("DIALOG_BUY_A_COFFEE");
                break;
            case DIALOG_EXIT_APP:
                dialog = SIVDialog.newInstance(
                        getString(R.string.title_exit_the_app),
                        getString(R.string.modal_exit_the_app_content),
                        getString(R.string.button_ok),
                        getString(R.string.button_exit),
                        DialogActions.EXIT,
                        DIALOG_EXIT_APP);
                mFirebaseHelper.logModalShows("DIALOG_EXIT_APP");
                break;
            default:
                break;
        }
        dialog.show(fm, String.valueOf(tag));

        Log.d(TAG, "openDialog TAG:" + tag);
    }

    public static Context getAppContext() {
        return mContext;
    }

}
