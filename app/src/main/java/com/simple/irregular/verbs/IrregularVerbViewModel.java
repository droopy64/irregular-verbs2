package com.simple.irregular.verbs;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class IrregularVerbViewModel extends AndroidViewModel {

    private IrregularVerbRepository repository;
    private LiveData<List<IrregularVerb>> allIrregularVerbs;

    public IrregularVerbViewModel(@NonNull Application application) {
        super(application);
        repository = new IrregularVerbRepository(application);
        allIrregularVerbs = repository.getAllIrregularVerbs();
    }

    public void insert(IrregularVerb irregularVerb) {
        repository.insert(irregularVerb);
    }

    public LiveData<List<IrregularVerb>> getAllIrregularVerbs() {
        return allIrregularVerbs;
    }
}
