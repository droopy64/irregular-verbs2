package com.simple.irregular.verbs;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class IrregularVerbRepository {
    private IrregularVerbDao irregularVerbDao;
    private LiveData<List<IrregularVerb>> allIrregularVerbs;

    public IrregularVerbRepository(Application application) {
        IrregularVerbDatabase database = IrregularVerbDatabase.getInstance(application);
        irregularVerbDao = database.irregularVerbDao();
        allIrregularVerbs = irregularVerbDao.getAllIrregularVerbs();
    }

    public void insert(IrregularVerb irregularVerb) {
        new InsertIrregularVerbAsyncTask(irregularVerbDao).execute(irregularVerb);
    }

    public void update(IrregularVerb irregularVerb) {
        new UpdateIrregularVerbAsyncTask(irregularVerbDao).execute(irregularVerb);
    }

    public void delete(IrregularVerb irregularVerb) {
        new DeleteIrregularVerbAsyncTask(irregularVerbDao).execute(irregularVerb);
    }

    public void deleteAllIrregularVerbs() {
        new DeleteAllIrregularVerbAsyncTask(irregularVerbDao).execute();

    }

    public LiveData<List<IrregularVerb>> getAllIrregularVerbs() {
        return allIrregularVerbs;
    }

    private static class InsertIrregularVerbAsyncTask extends AsyncTask<IrregularVerb,Void,Void> {
        private IrregularVerbDao irregularVerbDao;
        private InsertIrregularVerbAsyncTask(IrregularVerbDao irregularVerbDao){
            this.irregularVerbDao = irregularVerbDao;

        }

        @Override
        protected Void doInBackground(IrregularVerb... irregularVerbs) {
            irregularVerbDao.insert(irregularVerbs[0]);
            return null;
        }
    }

    private static class UpdateIrregularVerbAsyncTask extends AsyncTask<IrregularVerb,Void,Void> {
        private IrregularVerbDao irregularVerbDao;
        private UpdateIrregularVerbAsyncTask(IrregularVerbDao irregularVerbDao){
            this.irregularVerbDao = irregularVerbDao;

        }

        @Override
        protected Void doInBackground(IrregularVerb... irregularVerbs) {
            irregularVerbDao.update(irregularVerbs[0]);
            return null;
        }
    }

    private static class DeleteIrregularVerbAsyncTask extends AsyncTask<IrregularVerb,Void,Void> {
        private IrregularVerbDao irregularVerbDao;
        private DeleteIrregularVerbAsyncTask(IrregularVerbDao irregularVerbDao){
            this.irregularVerbDao = irregularVerbDao;

        }

        @Override
        protected Void doInBackground(IrregularVerb... irregularVerbs) {
            irregularVerbDao.delete(irregularVerbs[0]);
            return null;
        }
    }

    private static class DeleteAllIrregularVerbAsyncTask extends AsyncTask<Void,Void,Void> {
        private IrregularVerbDao irregularVerbDao;
        private DeleteAllIrregularVerbAsyncTask(IrregularVerbDao irregularVerbDao){
            this.irregularVerbDao = irregularVerbDao;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            irregularVerbDao.deleteAllIrregularVerbs();
            return null;
        }
    }
}
