package com.simple.irregular.verbs;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseHelper {
    private static final String TAG = "FirebaseHelper";
    private FirebaseAnalytics mFirebaseAnalytics;
    private Context mContext = MainActivity.getAppContext();

    public FirebaseHelper() {
        this.mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
    }

    public void setUserProperty(String key, String value) {
        mFirebaseAnalytics.setUserProperty(key, value);
    }

    public void logButtonItem(String buttonName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, buttonName);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void logModalShows(String modalName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "modal");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, modalName);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void logAdSource(String adSource) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "ad_source");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, adSource);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void logScreenName(String screenName, String className) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, screenName);
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, className);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

}
