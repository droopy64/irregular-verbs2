package com.simple.irregular.verbs;

public enum DialogActions {
    OK,
    CANCEL,
    BUY_COFFEE,
    EXIT
}
