package com.simple.irregular.verbs;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {IrregularVerb.class}, version = 1, exportSchema = false)
public abstract class IrregularVerbDatabase extends RoomDatabase {

    private static IrregularVerbDatabase instance;

    public abstract IrregularVerbDao irregularVerbDao();

    public static synchronized IrregularVerbDatabase getInstance(Context context){
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    IrregularVerbDatabase.class, "irregularverb_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback(){
        /**
         * Called when the database is created for the first time. This is called after all the
         * tables are created.
         *
         * @param db The database.
         */
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            new PopulateDbAsyncTask(instance).execute();
            super.onCreate(db);
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private IrregularVerbDao irregularVerbDao;

        private PopulateDbAsyncTask(IrregularVerbDatabase db) {
            irregularVerbDao = db.irregularVerbDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            irregularVerbDao.insert(new IrregularVerb("beat", "beat", "beaten"));
            irregularVerbDao.insert(new IrregularVerb("become", "became", "become"));
            irregularVerbDao.insert(new IrregularVerb("begin", "began", "begun"));
            irregularVerbDao.insert(new IrregularVerb("bend", "bent", "bent"));
            irregularVerbDao.insert(new IrregularVerb("bet", "bet", "bet"));
            irregularVerbDao.insert(new IrregularVerb("bite", "bit", "bitten"));
            irregularVerbDao.insert(new IrregularVerb("bleed", "bled", "bled"));
            irregularVerbDao.insert(new IrregularVerb("blow", "blew", "blown"));
            irregularVerbDao.insert(new IrregularVerb("break", "broke", "broken"));
            irregularVerbDao.insert(new IrregularVerb("breed", "bred", "bred"));
            irregularVerbDao.insert(new IrregularVerb("bring", "brought", "brought"));
            irregularVerbDao.insert(new IrregularVerb("build", "built", "built"));
            irregularVerbDao.insert(new IrregularVerb("burn", "burnt/burned", "burnt/burned"));
            irregularVerbDao.insert(new IrregularVerb("buy", "bought", "bought"));
            irregularVerbDao.insert(new IrregularVerb("catch", "caught", "caught"));
            irregularVerbDao.insert(new IrregularVerb("choose", "chose", "chosen"));
            irregularVerbDao.insert(new IrregularVerb("come", "came", "come"));
            irregularVerbDao.insert(new IrregularVerb("cost", "cost", "cost"));
            irregularVerbDao.insert(new IrregularVerb("cut", "cut", "cut"));
            irregularVerbDao.insert(new IrregularVerb("do", "did", "done"));
            irregularVerbDao.insert(new IrregularVerb("dig", "dug", "dug"));
            irregularVerbDao.insert(new IrregularVerb("draw", "drew", "drawn"));
            irregularVerbDao.insert(new IrregularVerb("dream", "dreamt/dreamed", "dreamt/dreamed"));
            irregularVerbDao.insert(new IrregularVerb("drink", "drank", "drunk"));
            irregularVerbDao.insert(new IrregularVerb("drive", "drove", "driven"));
            irregularVerbDao.insert(new IrregularVerb("eat", "ate", "eaten"));
            irregularVerbDao.insert(new IrregularVerb("fall", "fell", "fallen"));
            irregularVerbDao.insert(new IrregularVerb("feed", "fed", "fed"));
            irregularVerbDao.insert(new IrregularVerb("feel", "felt", "felt"));
            irregularVerbDao.insert(new IrregularVerb("fight", "fought", "fought"));
            irregularVerbDao.insert(new IrregularVerb("find", "found", "found"));
            irregularVerbDao.insert(new IrregularVerb("fly", "flew", "flown"));
            irregularVerbDao.insert(new IrregularVerb("forget", "forgot", "forgotten"));
            irregularVerbDao.insert(new IrregularVerb("forgive", "forgave", "forgiven"));
            irregularVerbDao.insert(new IrregularVerb("freeze", "froze", "frozen"));
            irregularVerbDao.insert(new IrregularVerb("get", "got", "got"));
            irregularVerbDao.insert(new IrregularVerb("give", "gave", "given"));
            irregularVerbDao.insert(new IrregularVerb("go", "went", "gone"));
            irregularVerbDao.insert(new IrregularVerb("grow", "grew", "grown"));
            irregularVerbDao.insert(new IrregularVerb("have", "had", "had"));
            irregularVerbDao.insert(new IrregularVerb("hear", "heard", "heard"));
            irregularVerbDao.insert(new IrregularVerb("hide", "hid", "hidden"));
            irregularVerbDao.insert(new IrregularVerb("hit", "hit", "hit"));
            irregularVerbDao.insert(new IrregularVerb("hold", "held", "held"));
            irregularVerbDao.insert(new IrregularVerb("hurt", "hurt", "hurt"));
            irregularVerbDao.insert(new IrregularVerb("keep", "kept", "kept"));
            irregularVerbDao.insert(new IrregularVerb("know", "knew", "known"));
            irregularVerbDao.insert(new IrregularVerb("lay", "laid", "laid"));
            irregularVerbDao.insert(new IrregularVerb("lead", "led", "led"));
            irregularVerbDao.insert(new IrregularVerb("lean", "leant/leaned", "leant/leaned"));
            irregularVerbDao.insert(new IrregularVerb("leave", "left", "left"));
            irregularVerbDao.insert(new IrregularVerb("lend", "lent", "lent"));
            irregularVerbDao.insert(new IrregularVerb("let", "let", "let"));
            irregularVerbDao.insert(new IrregularVerb("lose", "lost", "lost"));
            irregularVerbDao.insert(new IrregularVerb("make", "made", "made"));
            irregularVerbDao.insert(new IrregularVerb("mean", "meant", "meant"));
            irregularVerbDao.insert(new IrregularVerb("meet", "met", "met"));
            irregularVerbDao.insert(new IrregularVerb("pay", "paid", "paid"));
            irregularVerbDao.insert(new IrregularVerb("put", "put", "put"));
            irregularVerbDao.insert(new IrregularVerb("quit", "quit", "quit"));
            irregularVerbDao.insert(new IrregularVerb("read /ri:d/", "read /red/", "read /red/"));
            irregularVerbDao.insert(new IrregularVerb("ride", "rode", "ridden"));
            irregularVerbDao.insert(new IrregularVerb("ring", "rang", "rung"));
            irregularVerbDao.insert(new IrregularVerb("rise", "rose", "risen"));
            irregularVerbDao.insert(new IrregularVerb("run", "ran", "run"));
            irregularVerbDao.insert(new IrregularVerb("say", "said", "said"));
            irregularVerbDao.insert(new IrregularVerb("see", "saw", "seen"));
            irregularVerbDao.insert(new IrregularVerb("sell", "sold", "sold"));
            irregularVerbDao.insert(new IrregularVerb("send", "sent", "sent"));
            irregularVerbDao.insert(new IrregularVerb("set", "set", "set"));
            irregularVerbDao.insert(new IrregularVerb("shake", "shook", "shaken"));
            irregularVerbDao.insert(new IrregularVerb("shine", "shone", "shone"));
            irregularVerbDao.insert(new IrregularVerb("shoe", "shod", "shod"));
            irregularVerbDao.insert(new IrregularVerb("shoot", "shot", "shot"));
            irregularVerbDao.insert(new IrregularVerb("show", "showed", "shown"));
            irregularVerbDao.insert(new IrregularVerb("shrink", "shrank", "shrunk"));
            irregularVerbDao.insert(new IrregularVerb("shut", "shut", "shut"));
            irregularVerbDao.insert(new IrregularVerb("sing", "sang", "sung"));
            irregularVerbDao.insert(new IrregularVerb("sink", "sank", "sunk"));
            irregularVerbDao.insert(new IrregularVerb("sit", "sat", "sat"));
            irregularVerbDao.insert(new IrregularVerb("sleep", "slept", "slept"));
            irregularVerbDao.insert(new IrregularVerb("speak", "spoke", "spoken"));
            irregularVerbDao.insert(new IrregularVerb("spend", "spent", "spent"));
            irregularVerbDao.insert(new IrregularVerb("spill", "spilt/spilled", "spilt/spilled"));
            irregularVerbDao.insert(new IrregularVerb("spread", "spread", "spread"));
            irregularVerbDao.insert(new IrregularVerb("speed", "sped", "sped"));
            irregularVerbDao.insert(new IrregularVerb("stand", "stood", "stood"));
            irregularVerbDao.insert(new IrregularVerb("steal", "stole", "stolen"));
            irregularVerbDao.insert(new IrregularVerb("stick", "stuck", "stuck"));
            irregularVerbDao.insert(new IrregularVerb("sting", "stung", "stung"));
            irregularVerbDao.insert(new IrregularVerb("stink", "stank", "stunk"));
            irregularVerbDao.insert(new IrregularVerb("swear", "swore", "sworn"));
            irregularVerbDao.insert(new IrregularVerb("sweep", "swept", "swept"));
            irregularVerbDao.insert(new IrregularVerb("swim", "swam", "swum"));
            irregularVerbDao.insert(new IrregularVerb("swing", "swung", "swung"));
            irregularVerbDao.insert(new IrregularVerb("take", "took", "taken"));
            irregularVerbDao.insert(new IrregularVerb("teach", "taught", "taught"));
            irregularVerbDao.insert(new IrregularVerb("tear", "tore", "torn"));
            irregularVerbDao.insert(new IrregularVerb("tell", "told", "told"));
            irregularVerbDao.insert(new IrregularVerb("think", "thought", "thought"));
            irregularVerbDao.insert(new IrregularVerb("throw", "threw", "thrown"));
            irregularVerbDao.insert(new IrregularVerb("understand", "understood", "understood"));
            irregularVerbDao.insert(new IrregularVerb("wake", "woke", "woken"));
            irregularVerbDao.insert(new IrregularVerb("wear", "wore", "worn"));
            irregularVerbDao.insert(new IrregularVerb("win", "won", "won"));
            irregularVerbDao.insert(new IrregularVerb("write", "wrote", "written"));
            return null;
        }
    }
}
