package com.simple.irregular.verbs;

import android.app.Activity;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

public class Tools {

    private static final String TAG = "MainActivity";

    private PowerManager pm;
    private boolean isScreenOn;
    private boolean screenDimmed;
    private Activity mMainActivity;

    public Tools(Activity mainActivity) {
        this.mMainActivity = mainActivity;
    }

    public boolean isScreenOn() {
        pm = (PowerManager) mMainActivity.getSystemService(Context.POWER_SERVICE);
        isScreenOn = pm.isInteractive();
        return isScreenOn;
    }

    public void setScreenDimmed(boolean screenDimmedStatus) {
        this.screenDimmed = screenDimmedStatus;
    }

    public boolean wasScreenDimmed(){
        return screenDimmed;
    }

}
