package com.simple.irregular.verbs;

public enum DialogType {
    DIALOG_EXIT_APP,
    DIALOG_BUY_A_COFFEE
}
