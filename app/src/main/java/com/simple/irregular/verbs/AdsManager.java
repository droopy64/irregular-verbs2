package com.simple.irregular.verbs;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

import java.util.Arrays;
import java.util.List;

public class AdsManager {
    private static final String TAG = "AdsManager";
    List<String> testDeviceIds = Arrays.asList("D60A4C12578A1545478EC9E183E495C1");

    public static InterstitialAd mInterstitialAd;
    public static RewardedAd rewardedAd;
    private static Context ctx;

    public static boolean rewardedAdShown;

    public AdsManager(Context ctx) {
        this.ctx = ctx;

        MobileAds.initialize(ctx, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });
    }

    public static RewardedAd createRewardedAds() {
        rewardedAd = new RewardedAd(ctx, "ca-app-pub-3940256099942544/5224354917");
        RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
            @Override
            public void onRewardedAdLoaded() {
                // Ad successfully loaded.
                Log.d(TAG, "createRewardedAds: Ad successfully loaded");
            }

            @Override
            public void onRewardedAdFailedToLoad(LoadAdError adError) {
                // Ad failed to load.
                Log.d(TAG, "createRewardedAds: Ad failed to load");
            }
        };
        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
        return rewardedAd;
    }

    public InterstitialAd createInterstitialAds() {
        AdRequest adRequest = new AdRequest.Builder().build();

        // Interstitials don't have layout so I need to create an object
        InterstitialAd interstitialAd = new InterstitialAd(ctx);
        String bannerId = ctx.getString(R.string.interstitial_ad_id);
        interstitialAd.setAdUnitId(bannerId);
        interstitialAd.loadAd(adRequest);
        return interstitialAd;
    }

    public void createAds(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                Toast.makeText(ctx, "BANNER Ad load error" + loadAdError.getCode(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Toast.makeText(ctx, "BANNER add is loaded", Toast.LENGTH_SHORT).show();
            }
        });

        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

        adView.loadAd(adRequest);
    }

    public static boolean wasRewardedAdShown(){
        return rewardedAdShown;
    }

    public static void setRewardedAdShown(boolean value){
        rewardedAdShown = value;
    }

}
