package com.simple.irregular.verbs;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "irregularVerbs_table")
public class IrregularVerb {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String infinitive;
    private String pastSimple;
    private String pastParticiple;

    public IrregularVerb(String infinitive, String pastSimple, String pastParticiple) {
        this.infinitive = infinitive;
        this.pastSimple = pastSimple;
        this.pastParticiple = pastParticiple;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getInfinitive() {
        return infinitive;
    }

    public String getPastSimple() {
        return pastSimple;
    }

    public String getPastParticiple() {
        return pastParticiple;
    }
}
