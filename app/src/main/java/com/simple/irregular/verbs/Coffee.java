package com.simple.irregular.verbs;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;

public class Coffee {
    public static final String MyPREFERENCES = "SharedPrefs" ;
    private static final String TAG = "COFFEE";
    //private static Context ctx;
    private int counter;

    public Coffee() {
        //this.ctx = ctx;
    }

    public void addCoffee() {
        Context context = MainActivity.getAppContext();
        SharedPreferences sharedpreferences =  context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        counter = sharedpreferences.getInt("COFFEE_COUNTER", 0);
        counter++;
        editor.putInt("COFFEE_COUNTER", counter);
        editor.commit();
        Log.d(TAG,"COFFEE COUNTER INCREASED = " + counter);
    }

    public void removeCoffee(){
        // idea:
        // Small gamification feature - > user opens the app -> counter--:
        // decrement coffee counter if user did't see the add till the end.

        Context context = MainActivity.getAppContext();
        SharedPreferences sharedpreferences =  context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        counter = sharedpreferences.getInt("COFFEE_COUNTER", 0);
        counter--;
        editor.putInt("COFFEE_COUNTER", counter);
        editor.commit();
        Log.d(TAG,"COFFEE COUNTER DECREASED = " + counter);
    }

    public int getCounter() {
        Context context = MainActivity.getAppContext();
        SharedPreferences sharedpreferences =  context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        counter = sharedpreferences.getInt("COFFEE_COUNTER", 0);
        Log.d(TAG,"getCounter = " + counter);
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
